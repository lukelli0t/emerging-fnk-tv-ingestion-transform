const dynasty = require("dynasty")({ region: "us-east-1" });
const entities = require("html-entities").AllHtmlEntities;
const striptags = require("striptags");
const xmlBuilder = require("xmlbuilder");
const fs = require("fs");

const EPISODES_TABLE = "emerging-fnk-alexa-vsk-content-episode"
const TV_SHOWS_TABLE = "emerging-fnk-alexa-vsk-content-show"
const ENCODING = { encoding: "utf-8" };
const CATALOG = {
  Catalog: {
    "@xmlns": "http://www.amazon.com/FireTv/2014-04-11/ingestion",
    "@version": "FireTv-v1.3"
  }
}

const UTIL = {
  decodeDescription(description) {
    if (!description) return "";
    const decoded = entities.decode(description);
    const regex = new RegExp(/(<p>|<P>|<br>|<BR>|<h[1-5]>|<H[1-5]>)/)
    const decodedDescription = decoded.split(regex)[0];
    return striptags(decodedDescription)
  },
  getTvShowSource(sources = []) {
    return sources.find(s => s.type === "show")
  },
  getImageUrl(show) {
    if (show.image && show.image.url) {
      return show.image.url
    }
    if (show.images && show.images.logo && show.images.logo.url) {
      return show.images.logo.url
    }
    return ''
  }
}


async function transform() {
  try {
    const episodes = await dynasty.table(EPISODES_TABLE).scan();
    const shows = await dynasty.table(TV_SHOWS_TABLE).scan();
    let builder = xmlBuilder.create(CATALOG, ENCODING).ele("Works")

    shows.forEach((show, i) => {
      const description = UTIL.decodeDescription(show.description)
      const credits = builder.ele("TvShow")
        .ele("ID", show.id).up()
        .ele("Title", { locale: "en-US" }, show.title).up()
        .ele("Offers").ele("FreeOffer").ele("Regions").ele("Country", "US").up().up().up().up()
        .ele("ImageUrl", UTIL.getImageUrl(show)).up()
        .ele("ShortDescription", description).up()
        .ele('Credits')
      show.talent.forEach(name => {
        credits.ele('CastMember')
          .ele('Name', { locale: 'en-US' }, name).up()
          .ele('Role', { locale: 'en-US' }, 'Self').up()
      })
      credits.up()
    })

    episodes.forEach((episode, i) => {
      const description = UTIL.decodeDescription(episode.description)
      const sourceTvShow = UTIL.getTvShowSource(episode.sources)

      if (!sourceTvShow) {
        const msg = "ERROR: EPISODE HAS NO TV SHOW"
        return console.log(msg, i, episode.id, JSON.stringify(episode))
      }

      builder.ele("TvEpisode")
        .ele("ID", episode.id).up()
        .ele("Title", { locale: "en-US" }, episode.title).up()
        .ele("Offers").ele("FreeOffer").ele("Regions").ele("Country", "US").up().up().up().up()
        .ele("ImageUrl", episode.image.url).up()
        .ele("ShortDescription", description).up()
        .ele("ShowID", sourceTvShow.id).up()
        .ele("SeasonInShow", episode.season).up()
        .ele("EpisodeInSeason", episode.number).up()
        .ele("OriginalAirDate", episode.originalAirTime.split(".")[0]).up()
    })


    // TODO: Upload xml to s3
    const xml = builder.end({ pretty: true })

    fs.writeFileSync("./tmp/tv-shows-episodes.xml", xml, err => {
      if (err) return console.log(err)
      console.log("XML was saved!");
    })

    return { statusCode: 200, body: xml };
  } catch (error) {
    console.log(error)
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: error.toString()
      })
    }
  }
}

module.exports = { transform };

transform()
